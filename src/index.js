import sinon from 'sinon'

export const withSandbox = test => {
	return async () => {
		const sandbox = sinon.createSandbox()
		try {
			await test(sandbox)
			sandbox.restore()
		}
		catch (ex) {
			sandbox.restore()
			throw ex
		}
	}
}