import { withSandbox } from '../src/index'

import sinon from 'sinon'
import chai, { expect } from 'chai'
import sinonChai from 'sinon-chai'

chai.use(sinonChai)

describe('Sinon Sandbox Wrapper', () => {

	let sandbox;

	beforeEach(() => {
		sandbox = sinon.createSandbox()
	})

	afterEach(() => {
		sandbox.restore()
	})

	it('runs a perfect test', async () => {
		const testStub = sandbox.stub()

		const testFunction = await withSandbox(testStub)
		await testFunction()

		expect(testStub).called
	})

	it('creates and restores sandbox', async () => {
		const testStub = sandbox.stub()
		const innerSandboxStub = { restore : sandbox.stub() }

		const createStub = sandbox.stub(sinon, 'createSandbox')
		createStub.returns(innerSandboxStub)

		const testFunction = await withSandbox(testStub)
		await testFunction()

		expect(testStub).called
		expect(createStub).called
		expect(innerSandboxStub.restore).called
	})

	it('restores sandbox as well if test fails', async () => {
		const testStub = sandbox.stub()
		testStub.throws(new Error('irrelevant'))

		const innerSandboxStub = { restore : sandbox.stub() }
		const createStub = sandbox.stub(sinon, 'createSandbox')
		createStub.returns(innerSandboxStub)

		const testFunction = await withSandbox(testStub)

		try {
			await testFunction()
		} catch(e) {
			expect(testStub).called
			expect(createStub).called
			expect(innerSandboxStub.restore).called
			return
		}
		expect.fail('an exception was expected')
	})
})